FROM python:latest
WORKDIR /app
ADD . /app
RUN pip3 install Flask
ENV NAME Mirzzie
CMD ["python3", "app.py"]
